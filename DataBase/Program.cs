﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient; //andmetega toimetamisel on seda vaja!!!

namespace DataBase
{ partial class Customer
    { public string FullName => CompanyName +" " + ContactName; }//kui sa siia teed, siis jääb se alles, muidu kirjutatakse klassis Customer see üle iga meie tehtud muudatusega.
    class Program
    {
        static void Main(string[] args)
        {
            NorthwindEntities DataBase = new NorthwindEntities();//uus muutuja, klass peaks olema ALATI suure tähega
            foreach (var x in DataBase.Categories) Console.WriteLine(x.CategoryName);

            //int? arv = 7;//tal võib olla ka mõni muu väärtus, sellenimi on nullable int
            //Nullable<int> teine = null;//nullable asjad saavad olema AINULT arvud, võib ka nii kirjutada ülemist
            //arv = 0;
            //arv = null;
            
            //Console.WriteLine(  arv??-1);//otsi arv, vaata kas tal on väärtust, KUI tal pole väärtust anna talle asendusväärtus -1. Asendusväärtus peab olema sama tüüpi. Vüi kasutad convertimisväärtust
            
        }
    }
}
